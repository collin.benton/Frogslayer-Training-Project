# ------------------------------------------------------------------------------
# Collin Benton
# Lexer Version of C Code Reformatter
# 7/26/2019
#
# Lexer Rules: defines the rules that govern how the lexer will be built, and
# tracks info about current tokens and saves it as metadata to that token.
# ------------------------------------------------------------------------------

import ply.lex as lex

##------------------------------------------------------------------------------
## Lexer Rules
##------------------------------------------------------------------------------

# global variables
control_struct_flag = False
# control_struct_flag:
#	- True: for loop or control structure has been detected without an open
#			bracket
#	- False: no control structure with no open bracket

indent_flag = 0
# indent_flag:
#	0 -> no action needed
#	1 -> indentation has occurred, via control struct
#	2 -> one line with no open bracket detected
#	3 -> two lines with no open bracket detected, indentation must deccrease

indent_counter = 0
# indent_counter:
#	counts the number of indents that have occurred via
#		- lone open brackets
#		- control structures
#		- for loops

# list of token names (always required)
tokens = (
	'FOR_LOOP',
	'CONTROL_STRUCTURE',
	'OPEN_BRACKET',
	'CLOSE_BRACKET',
	'PARENTHESIS',
	'SINGLE_LINE_COMMENT',
	'MULTI_LINE_COMMENT',
	'NEWLINE',
	'WHITESPACE',
	'STRING',
	'OPERAND',
	'CODE'
)
##------------------------------------------------------------------------------
## Regular expression Token rules with action code
##------------------------------------------------------------------------------

# Define For Loop Token
def t_FOR_LOOP(t):
	r'\bfor\b'
	global indent_counter, indent_flag, control_struct_flag
	control_struct_flag = True
	indent_counter += 1
	if (indent_flag != 2):
		indent_flag = 1
	t.indent_count = indent_counter
	return t

# Define Control Structure Token
def t_CONTROL_STRUCTURE(t):
	r'\bwhile\b|\bfor\b|\bif\b|\bswitch\b|\bbreak\b|\bcontinue\b|\bgoto\b'
	global indent_counter, indent_flag, control_struct_flag
	control_struct_flag = True
	indent_counter += 1
	if (indent_flag != 2):
		indent_flag = 1
	t.indent_count = indent_counter
	return t

# Define Open Bracket Token
def t_OPEN_BRACKET(t):
	r'\{'
	global indent_counter, indent_flag, control_struct_flag
	indent_flag = 0
	if (not control_struct_flag):
		indent_counter += 1
	t.indent_count = indent_counter
	return t

# Define Close Bracket Token
def t_CLOSE_BRACKET(t):
	r'\}'
	global indent_counter, control_struct_flag
	control_struct_flag = False
	if (indent_counter > 0):
		indent_counter -= 1
	t.indent_count = indent_counter
	return t

# Define Parenthesis Token
def t_PARENTHESIS(t):
	r'\(|\)'
	global indent_counter
	t.indent_count = indent_counter
	return t

# Define Single Line Comment Token
def t_SINGLE_LINE_COMMENT(t):
	r'//[^\n]*'
	global indent_counter
	t.indent_count = indent_counter
	return t

# Define Multi Line Comment Token
def t_MULTI_LINE_COMMENT(t):
	r'(?s)/\\*.*?\\*/'
	global indent_counter
	t.indent_count = indent_counter
	return t

# Define Newline Token
def t_NEWLINE(t):
	r'\n[_,\t]*'
	t.lexer.lineno += 1
	global indent_counter, indent_flag
	if (indent_flag > 0):
		indent_flag += 1
	if (indent_flag == 3):
		if (indent_counter > 0):
			indent_counter -= 1
		indent_flag = 0
	t.indent_count = indent_counter
	return t

# Define Whitespace Token
def t_WHITESPACE(t):
	r'[ \t]+'
	global indent_counter
	t.indent_count = indent_counter
	return t

# Define String Token
def t_STRING(t):
	r'".*?"'
	global indent_counter
	t.indent_count = indent_counter
	return t

# Define Operand Token
def t_OPERAND(t):
	r'\+=|-=|/=|\*=|<<|>>|==|>|<|>=|<=|!=|&&|!|\|\||&|\||~|='
	global indent_counter
	t.indent_count = indent_counter
	return t

# Define Code Token
def t_CODE(t):
	r'[^ \{\}\(\)\n\t><=!&\|~]+'
	global indent_counter
	t.indent_count = indent_counter
	return t

# Define Error Handling Token
def t_error(t):
	print("Illegal character '%s'" % t.value[0])
	t.lexer.skip(1)

# Build & Return Lexer
def make_lexer():
	return lex.lex()