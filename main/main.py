# ------------------------------------------------------------------------------
# Collin Benton
# Lexer Version of C Code Reformatter
# 7/26/2019
# 
# Main Runner: Builds the lexer and loops through it, applying the appropriate
# rules for each token.
# ------------------------------------------------------------------------------

from lexer import make_lexer
from rules import *
from spellchecker.spellcheck_wrapper import check_spelling
import sys
import ply.lex as lex

##------------------------------------------------------------------------------
## input_file_from_cmd
##------------------------------------------------------------------------------

def output_results_to_file(filename, output_text):
	output_file = open('reformatted_'+filename, 'w+')
	output_file.write(output_text)

##------------------------------------------------------------------------------
## input_file_from_cmd
##------------------------------------------------------------------------------

def input_file_from_cmd():
	# get file from command line
	if (len(sys.argv) != 2):
		print('Usage: main.py <filename>')
		exit()

	filename = sys.argv[1]
	file = open(filename)
	return file

##------------------------------------------------------------------------------
## Runner
##------------------------------------------------------------------------------

def main():
	# Get input
	input_file = input_file_from_cmd()

	# Build the lexer
	lexer = make_lexer()
	lexer.input(input_file.read())

	# Declare flags
	newline_flag = False
	#	- True: an extra new line is needed
	#	- False: an extra new line is not needed
	indent_flag = False
	#	- True: indentation is needed
	#	- False: indentation is not needed
	space_flag = False
	#	- True: a space is needed
	#	- False: a space is not needed

	# Declare string constructor variables
	reformatted_text = ''
	#	- Stores the combined reformatted text
	reformatted_line = ''
	#	- Aggregates reformatted text into a line
	comment_text = ''
	#	- Stores all comments
	
	# Declare initial state
	state = [reformatted_line, '', indent_flag, space_flag]
	#	[0] - current line
	#	[1] - current token
	#	[2] - indentation flag
	#	[3] - space flag


	for tok in lexer:
		# Update state with new token
		state[1] = tok
		
		# Handle indentation after a new line
		if (indent_flag and tok.type != 'WHITESPACE'):
			state = indent_rule(state)
			indent_flag = False

		# Add an extra space when needed by space flag
		# (from bracket, parenthesis, & operand rule)
		elif (state[3]):
			if (tok.type != 'WHITESPACE'):
				state[0] += ' '
			# Set space_flag
			state[3] = False

		# Ignore existing indentation
		if (newline_flag and tok.type == 'WHITESPACE'):
			newline_flag = False

		# Add newline and aggregate reformatted line
		elif (tok.type == 'NEWLINE'):
			newline_flag = True
			indent_flag = True
			state[0] += '\n'
			reformatted_text += state[0]
			state[0] = ''
		
		# Bracket Token Case
		elif (tok.type == 'OPEN_BRACKET' or tok.type == 'CLOSE_BRACKET'):
			state = bracket_rule(state)
		
		# Parenthesis Token Case
		elif (tok.type == 'PARENTHESIS'):
			state = parenthesis_rule(state)
		
		# For Loop Token Case
		elif (tok.type == 'FOR_LOOP'):
			state = for_loop_rule(state)
		
		# Operand Token Case
		elif (tok.type == 'OPERAND'):
			state = operand_rule(state)
		
		# Comment Token Case
		elif (tok.type.find('COMMENT') > -1):
			tuple_out = comment_rule(state, comment_text)
			state = tuple_out[0]
			comment_text = tuple_out[1]
		
		# If no rule is needed, add current token to output
		else:
			newline_flag = False
			state[0] += str(tok.value)

	# Check spelling in comments and write to comment file
	comment_file = open('comments_' + input_file.name, 'w+')
	comment_text = check_spelling(comment_text)
	comment_file.write(comment_text)

	# Close files & output results to new file
	input_file.close()
	comment_file.close()
	output_results_to_file(input_file.name, reformatted_text)

if __name__ == '__main__':
	main()