# lextab.py. This file automatically created by PLY (version 3.11). Don't edit!
_tabversion   = '3.10'
_lextokens    = set(('CLOSE_BRACKET', 'CODE', 'CONTROL_STRUCTURE', 'FOR_LOOP', 'MULTI_LINE_COMMENT', 'NEWLINE', 'OPEN_BRACKET', 'OPERAND', 'PARENTHESIS', 'SINGLE_LINE_COMMENT', 'STRING', 'WHITESPACE'))
_lexreflags   = 64
_lexliterals  = ''
_lexstateinfo = {'INITIAL': 'inclusive'}
_lexstatere   = {'INITIAL': [('(?P<t_FOR_LOOP>\\bfor\\b)|(?P<t_CONTROL_STRUCTURE>\\bwhile\\b|\\bfor\\b|\\bif\\b|\\bswitch\\b|\\bbreak\\b|\\bcontinue\\b|\\bgoto\\b)|(?P<t_OPEN_BRACKET>\\{)|(?P<t_CLOSE_BRACKET>\\})|(?P<t_PARENTHESIS>\\(|\\))|(?P<t_SINGLE_LINE_COMMENT>//[^\\n]*)|(?P<t_MULTI_LINE_COMMENT>(?s)/\\\\*.*?\\\\*/)|(?P<t_NEWLINE>\\n[_,\\t]*)|(?P<t_WHITESPACE>[ \\t]+)|(?P<t_STRING>".*?")|(?P<t_OPERAND>\\+=|-=|/=|\\*=|<<|>>|==|>|<|>=|<=|!=|&&|!|\\|\\||&|\\||~|=)|(?P<t_CODE>[^ \\{\\}\\(\\)\\n\\t><=!&\\|~]+)', [None, ('t_FOR_LOOP', 'FOR_LOOP'), ('t_CONTROL_STRUCTURE', 'CONTROL_STRUCTURE'), ('t_OPEN_BRACKET', 'OPEN_BRACKET'), ('t_CLOSE_BRACKET', 'CLOSE_BRACKET'), ('t_PARENTHESIS', 'PARENTHESIS'), ('t_SINGLE_LINE_COMMENT', 'SINGLE_LINE_COMMENT'), ('t_MULTI_LINE_COMMENT', 'MULTI_LINE_COMMENT'), ('t_NEWLINE', 'NEWLINE'), ('t_WHITESPACE', 'WHITESPACE'), ('t_STRING', 'STRING'), ('t_OPERAND', 'OPERAND'), ('t_CODE', 'CODE')])]}
_lexstateignore = {'INITIAL': ''}
_lexstateerrorf = {'INITIAL': 't_error'}
_lexstateeoff = {}
