# ------------------------------------------------------------------------------
# Collin Benton
# Lexer Version of C Code Reformatter
# 7/26/2019
# 
# Parsing Rules: Defines the rule for each particular tokenen when found while
# parsing the tokenens.
# ------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## Formatting Rules
##------------------------------------------------------------------------------

# Indent Rule:
# 	Add indentation based on current token. If the current token is a control
#	statement, reduce indentation by 1 indent.
def indent_rule(state):
	line = state[0]
	token = state[1]

	# Mod to handle case for control statements
	mod = 0

	if (token.type == 'FOR_LOOP' or \
		token.type == 'CONTROL_STRUCTURE' or \
		token.type == 'OPEN_BRACKET'):
		mod = 1

	line += '\t' * (token.indent_count - mod)

	# Set indent_flag to false
	state[2] = False

	state[0] = line
	return state

# Bracket Rule:
# 	If any non space character is on the current line, add a new line, indent
#	it, and add the bracket to that new line.
def bracket_rule(state):
	line = state[0]
	token = state[1]

	if (not line.isspace()):
		state[0] += '\n'
		state = indent_rule(state)
		state[0] += str(token.value)

	else:
		state[0] += str(token.value)

	return state

# Parenthesis Rule:
#	If no space is before the parenthesis, add one, and set the flag to add a
# 	space after the parenthesis if needed.
def parenthesis_rule(state):
	line = state[0]
	token = state[1]

	# Set the space_flag
	state[3] = True
	
	if (line[len(line)-1] != ' '):
		line += ' ' + str(token.value)

	else:
		line += str(token.value)

	state[0] = line
	return state

# For Loop Rule:
#	Add a new line, then indent it, then add the for loop
def for_loop_rule(state):
	token = state[1]

	state[0] += '\n'
	state = indent_rule(state)
	state[0] += str(token.value)
	
	return state

# Operand Rule:
#	If there is not a space before an operand, add it and set the flag to add a
#	space after it.
def operand_rule(state):
	line = state[0]
	token = state[1]

	# Set the space_flag
	state[3] = True
	
	if (line[len(line)-1] != ' '):
		line += ' ' + token.value
	
	else:
		line += str(token.value)

	state[0] = line
	return state

# Comment Rule:
#	Add comment text to both the current line and the comment text.
def comment_rule(state, output_text):
	line = state[0]
	token = state[1]

	line += str(token.value)
	output_text += token.value + '\n'

	state[0] = line
	return (state, output_text)
