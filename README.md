# XYZ Tech C Code Reformatter

XYZ Tech C Code Reformatter will take a single file of c code as input, and
output the same code, but in a different, predetermined format. The format
specifications are listed below.

## Format Specifications

1. Fix Indentation - Code blocks will be indented with tabs according to
standard nesting guidelines.
2. Brackets - Brackets & Indentation will follow the Allman specification.
3. Parenthesis - Each parenthesis will have at least one space before and after
it.
4. For Loops - Each for loop will have at least one blank line preceeding it.
5. Comments - Comments will be saved to a new file and spellchecked within.
6. Benchmark Tests - The results of Benchmark Tests for development will be
saved to a PostgreSQL Database.

## Usage

```python
python main.py [filename]
```
