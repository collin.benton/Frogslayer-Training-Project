# ------------------------------------------------------------------------------
# Collin Benton
# Lexer Version of C Code Reformatter
# 7/26/2019
# ------------------------------------------------------------------------------

from lexer import make_lexer
from rules import *
from spellchecker.spellcheck_wrapper import check_spelling
import sys
import ply.lex as lex

##------------------------------------------------------------------------------
## input_file_from_cmd
##------------------------------------------------------------------------------

def output_results_to_file(filename, output_text):
	output_file = open('reformatted_'+filename, 'w+')
	output_file.write(output_text)

##------------------------------------------------------------------------------
## input_file_from_cmd
##------------------------------------------------------------------------------

def input_file_from_cmd():
	# get file from command line
	if (len(sys.argv) != 2):
		print('Usage: main.py <filename>')
		exit()
	filename = sys.argv[1]
	file = open(filename)
	return file

##------------------------------------------------------------------------------
## Runner
##------------------------------------------------------------------------------

def main():
	# give lexer input

	# Get input
	file = input_file_from_cmd()

	# build the lexer
	lexer = make_lexer()
	lexer.input(file.read())

	newline_flag = False
	output_text = ''
	output_line = ''
	comment_text = ''
	comment_file = open('comments_' + file.name, 'w+')

	need_to_indent = False
	need_space = False
	state = [output_line, '', need_to_indent, need_space]
	# 		[output, token, indent_flag, space_flag]

	#lex_str = ''

	for tok in lexer:
		state[1] = tok
		#lex_str += str(tok)+' IC: '+ str(state[1].indent_count) + '\n'

		# Handle indentation after a new line
		if (need_to_indent and tok.type != 'WHITESPACE'):
			state = indent_rule(state)
			need_to_indent = False

		# Add an extra space when needed
		# (from bracket, parenthesis, & operand rule)
		elif (state[3]):
			if (tok.type != 'WHITESPACE'):
				state[0] += ' '
				#print(' ', end='')
			state[3] = False

		if (newline_flag and tok.type == 'WHITESPACE'):
			newline_flag = False

		elif (tok.type == 'NEWLINE'):
			newline_flag = True
			need_to_indent = True
			state[0] += '\n'
			output_text += state[0]
			state[0] = ''
			#print('\n', end='')
			#state[0] += (str)(tok.indent_count) + '\t'
		
		elif (tok.type == 'OPEN_BRACKET' or tok.type == 'CLOSE_BRACKET'):
			newline_flag = False
			state = bracket_rule(state)
		
		elif (tok.type == 'PARENTHESIS'):
			newline_flag = False
			state = parenthesis_rule(state)
		
		elif (tok.type == 'FOR_LOOP'):
			newline_flag = False
			state = for_loop_rule(state)
		
		elif (tok.type == 'OPERAND'):
			newline_flag = False
			state = operand_rule(state)
		
		elif (tok.type.find('COMMENT') > -1):
			newline_flag = False
			tuple_out = comment_rule(state, comment_text)
			state = tuple_out[0]
			comment_text = tuple_out[1]
			#print(comment_text)
		
		else: #if (tok.type != 'NEWLINE'):
			newline_flag = False
			state[0] += str(tok.value)
			#print(tok.value, end='')

	comment_text = check_spelling(comment_text)
	comment_file.write(comment_text)

	file.close()
	comment_file.close()
	output_results_to_file(file.name, output_text)
	#print('\n\n'+lex_str)
	#print('\n'+state[0])

if __name__ == '__main__':
	main()