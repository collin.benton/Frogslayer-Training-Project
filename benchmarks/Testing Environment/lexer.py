
import ply.lex as lex

##------------------------------------------------------------------------------
## Lexer Rules
##------------------------------------------------------------------------------

# global variables
control_struct_flag = False
# control_struct_flag:
#	T -> for loop or control structure has been detected without an open bracket
#	F -> no control structure with no open bracket

indent_flag = 0
# indent_flag:
#	0 -> no action needed
#	1 -> indentation has occurred, via control struct
#	2 -> one line with no open bracket detected
#	3 -> two lines with no open bracket detected, indentation must deccrease

indent_counter = 0
# indent_counter:
#	counts the number of indents that have occurred via
#		- lone open brackets
#		- control structures
#		- for loops

# list of token names (always required)
tokens = (
	'FOR_LOOP',
	'CONTROL_STRUCTURE',
	'OPEN_BRACKET',
	'CLOSE_BRACKET',
	'PARENTHESIS',
	'SINGLE_LINE_COMMENT',
	'MULTI_LINE_COMMENT',
	'NEWLINE',
	'WHITESPACE',
	'STRING',
	'OPERAND',
	'CODE'
)

# regular expression rules with action code
def t_FOR_LOOP(t):
	r'\bfor\b'
	global indent_counter, indent_flag, control_struct_flag
	control_struct_flag = True
	indent_counter += 1
	if (indent_flag != 2):
		indent_flag = 1
	t.indent_count = indent_counter
	
	#print('FOR: csf='+str(control_struct_flag)+' | if='+str(indent_flag)+' | ic='+str(indent_counter))
	return t

def t_CONTROL_STRUCTURE(t):
	r'\bwhile\b|\bfor\b|\bif\b|\bswitch\b|\bbreak\b|\bcontinue\b|\bgoto\b'
	global indent_counter, indent_flag, control_struct_flag
	control_struct_flag = True
	indent_counter += 1
	if (indent_flag != 2):
		indent_flag = 1
	t.indent_count = indent_counter
	#print('CST: csf='+str(control_struct_flag)+' | if='+str(indent_flag)+' | ic='+str(indent_counter))
	return t

def t_OPEN_BRACKET(t):
	r'\{'
	global indent_counter, indent_flag, control_struct_flag
	indent_flag = 0
	if (not control_struct_flag):
		indent_counter += 1
	t.indent_count = indent_counter
	
	#print('OBR: csf='+str(control_struct_flag)+' | if='+str(indent_flag)+' | ic='+str(indent_counter))
	return t

def t_CLOSE_BRACKET(t):
	r'\}'
	global indent_counter, control_struct_flag
	control_struct_flag = False
	if (indent_counter > 0):
		indent_counter -= 1
	t.indent_count = indent_counter
	return t

def t_PARENTHESIS(t):
	r'\(|\)'
	global indent_counter
	t.indent_count = indent_counter
	return t

def t_SINGLE_LINE_COMMENT(t):
	r'//[^\n]*'
	global indent_counter
	t.indent_count = indent_counter
	return t

def t_MULTI_LINE_COMMENT(t):
	r'(?s)/\\*.*?\\*/'
	global indent_counter
	t.indent_count = indent_counter
	return t

def t_NEWLINE(t):
	r'\n[_,\t]*'
	t.lexer.lineno += 1
	global indent_counter, indent_flag
	if (indent_flag > 0):
		indent_flag += 1
	if (indent_flag == 3):
		if (indent_counter > 0):
			indent_counter -= 1
		indent_flag = 0
	t.indent_count = indent_counter
	#print('NWL: csf='+str(control_struct_flag)+' | if='+str(indent_flag)+' | ic='+str(indent_counter))
	return t

def t_WHITESPACE(t):
	r'[ \t]+'
	global indent_counter
	t.indent_count = indent_counter
	return t

def t_STRING(t):
	r'".*?"'
	global indent_counter
	t.indent_count = indent_counter
	return t

def t_OPERAND(t):
	r'\+=|-=|/=|\*=|<<|>>|==|>|<|>=|<=|!=|&&|!|\|\||&|\||~|='
	global indent_counter
	t.indent_count = indent_counter
	return t

def t_CODE(t):
	r'[^ \{\}\(\)\n\t><=!&\|~]+'
	global indent_counter
	t.indent_count = indent_counter
	return t

# Error handling rule
def t_error(t):
	print("Illegal character '%s'" % t.value[0])
	t.lexer.skip(1)

def make_lexer():
	return lex.lex()