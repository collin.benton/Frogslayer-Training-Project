import re
import math
from grammarbot import GrammarBotClient
from textwrap import wrap

def create_spell_check_client():
	return GrammarBotClient()

def spell_check(client, text):
	return client.check(text)

def get_matches_from_response(response):
	return response.matches

def init_spell_checker(text):

	client = create_spell_check_client()
	response = spell_check(client, text)
	matches = get_matches_from_response(response)
	return matches

def check_spelling(text_to_check):
	if (len(text_to_check) == 0):
		return ''

	# if length of text > 50000 split it into 49999 sized chunks due to the
	#	api having a 50000 character limit per request
	output_text = ''
	text_to_check = ''.join(filter(None, text_to_check))
	text_blocks = wrap(text_to_check, 5000, replace_whitespace=False)
	# Holds a list of the ending output messages
	messages = []

	# for every set of 50000 characters
	for text in text_blocks:
		# matches: list of all flagged errors in the block
		matches = init_spell_checker(text)
		# number of typo/spelling errors
		num_matches = 1
		# added_mod accounts for the added text that flags the errors ex: <#>
		added_mod = -3

		for match in matches:
			#print(match)
			#print(match.message)
			# if the error is a spelling error
			if (match.category == 'TYPOS'):
				num_matches += 1
				# add a # of characters to the modifier by the # of digits + 2
				added_mod += 2 + len(str(num_matches))
				# handles the case where a digit is gained
				# if log base 10 of num_matches is an integer
				log_matches = math.log10(num_matches)
				if (log_matches % 2 == 0 or (log_matches+1) % 2 == 0):
					added_mod -= 1
				substr_start = match.replacement_offset + added_mod
				#substr_end = substr_start + match.replacement_length
				#print('\t' + str(substr_start) + '|' + str(substr_end) + ' == '+ text[substr_start:substr_end])
				text = text[:substr_start] + '<' + str(num_matches) + '>' + text[substr_start:len(text)]
				possible_fix = str(match.corrections[0])[match.replacement_offset:match.replacement_offset + match.replacement_length]
				messages.append('Suggested alternative(s): ' + ''.join(possible_fix.split()))
		output_text += text
		
	output_text += '\n\n// Suggested spell checking edits:\n\n'
	for i in range(len(messages)):
		output_text += '// ' + str(i+1) + '. ' + str(messages[i]) + '\n'

	return output_text
