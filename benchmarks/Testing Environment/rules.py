##------------------------------------------------------------------------------
## Formatting Rules
##------------------------------------------------------------------------------

def indent_rule(state): #TODO fix extra space after for loop
	#indent_list = ['FOR_LOOP', 'CONTROL_STRUCTURE', 'OPEN_BRACKET']
	mod = 0
	#print ('Indenting <<', state[1].value, '>>')

	if (state[1].type == 'FOR_LOOP' or \
		state[1].type == 'CONTROL_STRUCTURE' or \
		state[1].type == 'OPEN_BRACKET'):
		#print ('mod = 1')
		mod = 1

	#print('INDENTING:', state[1].indent_count - mod)
	state[0] += '\t' * (state[1].indent_count - mod)
	#print('\t' * (state[1].indent_count - mod), end='')
	state[2] = False
	return state

def bracket_rule(state): #TODO fix extra space after for loop
	line = state[0]
	
	if (not line.isspace()):
		state[0] += '\n'
		#print('\n', end='')
		state = indent_rule(state)
		state[0] += str(state[1].value)
		#print(state[1].value, end='')
	
	else:
		state[0] += str(state[1].value)
		#print(state[1].value, end='')

	return state

def parenthesis_rule(state):
	lines = state[0].splitlines(True)
	line = lines[len(lines)-1]
	state[3] = True
	
	if (line[len(line)-1] != ' '):
		state[0] += ' ' + str(state[1].value)
		#print(' ' + state[1].value, end='')

	else:
		state[0] += str(state[1].value)
		#print(state[1].value, end='')

	return state

def for_loop_rule(state):
	state[0] += '\n'
	#print('\n', end='')
	state = indent_rule(state)
	state[0] += str(state[1].value)
	#print(state[1].value, end='')
	return state

def operand_rule(state):
	lines = state[0].splitlines(True)
	line = lines[len(lines)-1]
	state[3] = True
	
	#print('**'+line[len(line)-1]+'**', end='')
	if (line[len(line)-1] != ' '):
		state[0] += ' ' + state[1].value
		#print(' ' + state[1].value, end='')
	
	else:
		state[0] += str(state[1].value)
		#print(state[1].value, end='')

	return state

def comment_rule(state, output_text):
	state[0] += str(state[1].value)
	#print(state[1].value, end='')
	output_text += state[1].value + '\n'
	return (state, output_text)