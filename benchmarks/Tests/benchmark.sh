#! /usr/bin/bash

printf 'testing 1000 lines...'
time python -O main.py 1000_benchmark.c

printf '\ntesting 5000 lines...'
time python -O main.py 5000_benchmark.c 

printf '\ntesting 10000 lines...'
time python -O main.py 10000_benchmark.c 

printf '\ntesting 50000 lines...'
time python -O main.py 50000_benchmark.c 

printf '\ntesting 100000 lines...'
time python -O main.py 100000_benchmark.c 

printf '\ntesting 500000 lines...'
time python -O main.py 500000_benchmark.c 

printf '\ntesting 1000000 lines...'
time python -O main.py 1000000_benchmark.c 

printf '\ntesting 2000000 lines...'
time python -O main.py 2000000_benchmark.c 

printf '\ntesting 3000000 lines...'
time python -O main.py 3000000_benchmark.c 